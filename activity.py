class Camper:
    def __init__(self, name, batch, course_type):
        self.name = name
        self.batch = batch
        self.course_type = course_type
    
    def career_track(self):
        print("Currently enrolled in the {} program.".format(self.course_type))
    
    def info(self):
        print("My name is {} of batch {}.".format(self.name, self.batch))
        
zuitt_camper = Camper("John", "Batch 10", "Web Development")

print("Camper Name:", zuitt_camper.name)
print("Camper Batch:", zuitt_camper.batch)
print("Camper Course:", zuitt_camper.course_type)

zuitt_camper.info()
zuitt_camper.career_track()
